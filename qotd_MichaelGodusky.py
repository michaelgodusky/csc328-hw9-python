#************************************************************
#
#      Author:             Michael Godusky
#      Major:              Information Technology
#      Creation Date:      11/08/2021
#      Due Date:           11/09/2021
#      Course:             CSC328
#      Professor Name:     Dr. Frye
#      Assignment:         TCP and UDP Client (QOTD) Homework
#      Filename:           qodt_MichaelGodusky.py
#      Purpose:            Understand TCP and UDP sockets
#                          and utilize them in a client
#                          program.
#
#      Note 1: All documentation can be found at:
#      https://docs.python.org/3.9/library/socket.html
#
#      Note 2: This program was made and tested on a laptop
#      running Ubuntu 21.10 natively along with python 3.9.7
#
#      Note 3: When running this program on my own terminal
#      enviroment (not on acad) I was able to get QOTD from
#      alpha.mike-r.com and djxmmx.net, but not acad. However,
#      when I uploaded the program to acad and ran all three
#      QOTD servers I was able to get acad's QOTD. I'm not
#      sure if I missed the heads up during class or not but
#      I wanted to mention it just in case it wasn't.
#
#************************************************************

import sys      # for accessing arguments
import errno    # for calling errors
import socket   # for accessing sockets

def arg_check(argv):
    """************************************************************
    *
    *   Function name:  arg_check
    *   Description:    Check if arg amount is correct and check
    *                   if the first arg is tcp or udp. Then return
    *                   the socket type.
    *   Parameters:     argv
    *   Return value:   socket type
    *
    ************************************************************"""
    # make sure the correct amount of args is give
    if len(sys.argv) < 3:
        print("Error: You need to enter two arguments.")
        print("Please enter either <tcp> or <udp> and a <hostname>.")
        errno.EINVAL # arg list is too small
        quit()
    elif len(sys.argv) > 3:
        print("Error: You need to enter two arguments.")
        print("Please enter either <tcp> or <udp> and a <hostname>.")
        errno.E2BIG # arg list is too long
        quit()

    # covert string to lower case
    socket_type = argv[1].lower()

    #if(argv[1].lower() != 'tcp' and argv[1].lower() != 'udp'):
    if(socket_type != 'tcp' and socket_type != 'udp'):
        print("Error: Invalid first argument.")
        print("Please enter either <tcp> or <udp> for your first argument.")
        errno.EINVAL # invalid arg
        quit()

    return socket_type

def get_address(argv):
    """************************************************************
    *
    *   Function name:  get_address
    *   Description:    Use the socket module to find the server 
    *                   ip address. If an address is found then
    *                   it's returned to main, but if no address
    *                   is found then an error message sent.
    *   Parameters:     argv
    *   Return value:   server ip address
    *
    ************************************************************"""
    # covert string to lower case
    hostname = argv[2].lower()

    # try to find server hostname and if no dice then print an error
    try:
        return socket.gethostbyname(hostname)
    except:
        print("Error: no ip address for that host name.")
        quit()

def get_qotd(socket_type, server_address):
    """************************************************************
    *
    *   Function name:  get_qotd
    *   Description:    Fetch the QOTD from the server.
    *   Parameters:     socket_type and server_address
    *   Return value:   none
    *
    ************************************************************"""
    # if/else to determine which socket to create
    if(socket_type == 'tcp'):
        try:
            # create a tcp connection
            tcp_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        
            # connect the socket to the server on port 17
            tcp_socket.connect((server_address, 17))

            # receive qotd from the server
            qotd_message = tcp_socket.recv(512)

            # print the qotd
            print(qotd_message)

            #close the socket
            tcp_socket.close()
        except:
            print("Error: Problem connecting to the QOTD server.")
            quit()
    else:
        try:
            #create a udp connection
            udp_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
            
            # send the server a blank message, 
            udp_socket.sendto(b'', (server_address, 17))

            # receive qotd from server and print it
            data, address = udp_socket.recvfrom(512)
            print(data.decode())

            #close the socket
            udp_socket.close()
        except:
            print("Error: Problem connecting to the QOTD server.")
            quit()

def main(argv):

    socket_type = arg_check(argv)           # get socket type
   
    server_address = get_address(argv)      # get server address

    get_qotd(socket_type, server_address)   # get QOTD

if __name__ == "__main__":
    main(sys.argv) 

